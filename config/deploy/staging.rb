# vagrant
server "192.168.33.10", :app, :web, :primary => true
set :rails_env, 'production'
set :domain, '192.168.33.10'
role :db, domain, :primary => true
