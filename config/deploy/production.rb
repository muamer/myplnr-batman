server "VPS_IP", :app, :web, :primary => true
set :domain, 'VPS_IP'
role :db, domain, :primary => true
set :rails_env, 'production'
