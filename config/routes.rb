Myplnr::Application.routes.draw do
  root :to => 'ember#index'

  namespace :api do
    namespace :v1 do
      resources :lists do
        member do
          post :update_quadrants
        end
      end
      resources :tasks
    end
  end
end
