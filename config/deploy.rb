require 'bundler/capistrano'
require 'capistrano/ext/multistage'

set :stages, %w(production staging)
set :default_stage, 'staging'

set :application, 'myplnr'
set :user, 'deployer'
set :deploy_to, "/var/www/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, true

set :bundle_without, [:development, :test, :acceptance]

set :scm, 'git'
set :repository, 'git@bitbucket.org:muamer/myplnr-batman.git'
set :branch, 'master'

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set :default_environment, {
  'PATH' => '/usr/local/rbenv/shims:/usr/local/rbenv/bin:$PATH'
}

after 'deploy', 'deploy:cleanup' # keep only the last 5 releases

after 'deploy:setup' do
  sudo "chown -R #{user} #{deploy_to} && chmod -R g+s #{deploy_to}"
end

namespace :deploy do
  desc <<-DESC
  Send a USR2 to the unicorn process to restart for zero downtime deploys.
  runit expects 2 to tell it to send the USR2 signal to the process.
  DESC

  #namespace :assets do
    #task :precompile, :roles => :web, :except => { :no_release => true } do
      #run "cd #{current_path} && #{rake} RAILS_ENV=#{rails_env} RAILS_GROUPS=assets assets:precompile --trace"
    #end
  #end

  namespace :db do
    desc "create databases"
    task :create do
      run "cd #{current_path}; bundle exec rake db:create RAILS_ENV=#{rails_env}"
    end

    desc "load schema"
    task :schema_load do
      run "cd #{current_path}; bundle exec rake db:schema:load RAILS_ENV=#{rails_env}"
    end
  end

  task :restart, :roles => :app, :except => { :no_release => true } do
    run "sv 2 /home/#{user}/service/#{application}"
  end
end
