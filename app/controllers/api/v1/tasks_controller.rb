class Api::V1::TasksController < ApplicationController
  # GET /:list_id/tasks
  def index
    @list = List.find(params[:list_id])
    render json: @list.tasks
  end

  # POST /:list_id/tasks
  # for validation, can't use responders (batman expects errors to not have a root)
  def create
    @list = List.find(params[:task][:list_id])
    @task = @list.tasks.build(params[:task])

    if @task.save!
      render json: @task
    else
      render json: @task.errors, status: unprocessable_entity
    end
  end

  # PUT /:list_id/tasks/1
  def update
    @task = Task.find(params[:id])
    if @task.update_attributes(params[:task])
      render json: @task
    else
      render json: @task, status: unprocessable_entity
    end
  end

  # DELETE /:list_id/tasks/1
  def destroy
    @task = Task.find(params[:id])

    if @task.destroy
      render json: @task
    else
      render json: @task, status: unprocessable_entity
    end
  end
end
