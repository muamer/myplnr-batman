class Api::V1::ListsController < ApplicationController
  def list_params
    params.require(:list).permit(:id, :title)
  end

  def index
    render json: List.all
  end

  def show
    render json: List.find(params[:id])
  end

  def create
    render json: List.create
  end

  def update_quadrants
    Task.transaction do
      params[:dirty_quadrants].each do |quadrant|
        quadrant[:tasks].each do |task_id, index|
          Task.update(task_id, {:quadrant => quadrant[:quadrant_id], :sort => index})
        end
      end
    end
  end
end
