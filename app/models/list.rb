class List < ActiveRecord::Base
  has_many :quadrants, :dependent => :destroy
  has_many :tasks, :through => :quadrants

  before_create :set_uuid
  before_save :create_quadrants

  def set_uuid
    self.id = SecureRandom.uuid
  end

  # UUID id converted to string, without dashes
  def get_uuid
    id.to_s.gsub!('-', '')
  end

  # Add 4 default quadrants to created list
  def create_quadrants
    (1..4).each {|i| quadrants << Quadrant.new(index: i)}
  end
end
