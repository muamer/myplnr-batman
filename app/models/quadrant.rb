class Quadrant < ActiveRecord::Base
  has_many :tasks, :dependent => :destroy
  belongs_to :list
end
