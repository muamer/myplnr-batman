class Task < ActiveRecord::Base
  validates :text, presence: true
  validates :list, presence: true
  validates :quadrant, presence: true
  validates_inclusion_of :completed, in: [true, false]

  default_scope -> { order('sort') }

  belongs_to :quadrant
  delegate :list, :to => :quadrant

  before_create :set_default_sort

  def set_default_sort
    self.sort = Time.now.to_i
  end
end
