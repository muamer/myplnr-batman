#
# Make it global
#
root = exports ? this
root.Flash = {}

#
# Flash Message model
#
Flash.Message = Ember.Object.extend
  mesage: null
  type: 'notice'

#
# Flash Messages collection
#
Flash.Messages = Ember.ArrayProxy.create
  content: []
  pushMessage: (message, type) ->
    @pushObject Flash.Message.create {message: message, type: type}


#
# Flash Messages Controller
#
Flash.MessagesController = Em.ArrayController.create
  content: Flash.Messages

#
# Messages View
#
Flash.MessagesView = Ember.CollectionView.extend
  contentBinding: 'Flash.MessagesController.content'
  itemViewClass: Ember.View.extend({
    template: Ember.Handlebars.compile("{{view.content.message}}")
    classNames: ['alert-box']
    didInsertElement: ->
      setTimeout =>
        Flash.Messages.removeObject @get('content')
      , 5000
  })
