Myplnr.QuadrantView = Ember.View.extend
  tagName: 'ul'
  templateName: 'quadrant'
  contentBinding: 'controller'
  classNameBindings: ['quadrantClass']
  classNames: 'quadrant-wrapper tasks large-6 small-3 columns'
  quadrantClassBinding: 'controller.quadrantClass'

  didInsertElement: ->
    @syncHeight @.$()

  syncHeight: (quadrant) ->
    #available_height = $(window).height() - $('.top-bar').height()
    available_height = $(window).height() 
    $(quadrant).height available_height/2
    $(quadrant).css 'max-height', available_height/2
