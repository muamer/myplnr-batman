Myplnr.IndexController = Em.Controller.extend
  newList: ->
    list = Myplnr.List.createRecord()

    list.one 'didCreate', @, (list) ->
      @transitionToRoute('list', list)

    list.one 'becameError', @, ->
      Myplnr.setFlash('Cannot create list!', 'error')

    list.save()
