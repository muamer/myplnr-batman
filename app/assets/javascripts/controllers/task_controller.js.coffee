Myplnr.TaskController = Em.ObjectController.extend
  editing: false

  # start editing
  edit: ->
    @set('isEditing', true)

  # cancel editing
  cancel: ->
    @set('isEditing', false)

  submit: ->
    task = @get('model')
    task.one 'didUpdate', @, =>
      @set('isEditing', false)
    task.one 'becameError', @, ->
      Myplnr.setFlash('Cannot update task!', 'error')

    task.save()

  completeTask: ( ->
    task = @get('model')
    task.one 'becameError', @, =>
      Myplnr.setFlash('Cannot update task!', 'error')

    task.save()
  ).observes('completed')

