Myplnr.QuadrantController = Em.ObjectController.extend
  itemController: 'task'
  needs: 'list'

  showForm: false

  # show new task form
  showTaskForm: ->
    @set 'showForm', true

  # hide new task form
  cancel: ->
    @set 'showForm', false

  quadrantClass: ( ->
    "q#{@get('index')}"
  ).property('index')

  submit: (task) ->
    task = Myplnr.Task.createRecord
      text: @get('text')
      completed: @get('completed')
      list: @get('controllers.list.model')
      quadrant: @get('model')

    # reset form fields
    task.one 'didCreate', @, ->
      @set('text', '')
      @set('completed', false)

    task.one 'becameError', @, ->
      Myplnr.setFlash('Cannot create task!', 'error')
      
    task.save()


