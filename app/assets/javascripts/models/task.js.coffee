Myplnr.Task = DS.Model.extend
  text: DS.attr('string')
  quadrant: DS.attr('number')
  completed: DS.attr('boolean')
  sort: DS.attr('number')
  quadrant: DS.belongsTo('Myplnr.Quadrant')
  list: DS.belongsTo('Myplnr.List')
