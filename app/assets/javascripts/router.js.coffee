# For more information see: http://emberjs.com/guides/routing/

Myplnr.Router.map ()->
   @resource 'lists', ->
     @route 'new'
   @resource 'list', path: '/:list_id'

