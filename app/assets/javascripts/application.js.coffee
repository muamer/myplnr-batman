#= require jquery
#= require jquery_ujs
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require myplnr

root = exports ? this
root.Myplnr = Ember.Application.create()
