class ListSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id
  has_many :quadrants

  # Serialize UUID as string, not hex (default)
  def id
    object.get_uuid
  end
end
