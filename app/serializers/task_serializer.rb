class TaskSerializer < ActiveModel::Serializer
  attributes :id, :text, :quadrant, :completed, :sort
  has_one :list
end
