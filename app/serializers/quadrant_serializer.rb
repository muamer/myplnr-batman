class QuadrantSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :index
  has_many :tasks
end
