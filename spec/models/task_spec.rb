require "spec_helper"

describe Task do
  let(:task) do
    FactoryGirl.build(:task)
  end

  it "has a valid factory" do
    task.should be_valid
  end

  it "must have content" do
    task.content = nil
    expect(task).not_to be_valid
  end

  it "must have list_id" do
    task.list_id = nil
    expect(task).not_to be_valid
  end

  it "must have quadrant" do
    task.quadrant = nil
    expect(task).not_to be_valid
  end

  it "must have completed field" do
    task.completed = nil
    expect(task).not_to be_valid
  end
end
