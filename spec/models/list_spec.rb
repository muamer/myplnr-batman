require "spec_helper"

describe List do
  let(:list) do
    list = FactoryGirl.create(:list)
  end

  it 'has a valid factory' do
    list.should be_valid
  end

  it 'uuid should return UUID id as string' do
    list.get_uuid.class.should eq String
    list.get_uuid.length.should eq 36
  end

  context 'tasks counter cache' do
    let(:list) { FactoryGirl.create(:list, :with_tasks) }

    it 'updates tasks count after task save' do
      expect {
        FactoryGirl.create(:task, :list_id => list.id)
        list.reload
      }.to change(list, :tasks_count).by(1)
    end

    it 'updates tasks count after task destroy' do
      expect {
        list.tasks.last.destroy
        list.reload
      }.to change(list, :tasks_count).by(-1)
    end

    it 'updates completed tasks counter after task save' do
      expect {
        FactoryGirl.create(:task, :list_id => list.id, :completed => true)
        list.save
        list.reload
      }.to change(list, :completed_tasks_count).by(1)
    end

    it 'updates completed tasks counter after task update' do
      expect {
        task = list.tasks.last
        task.update_attributes(:completed => true)
        list.reload
      }.to change(list, :completed_tasks_count).by(1)
    end

    it "updates completed tasks counter after task destroy" do
      list.tasks.last.update_attributes(:completed => true)
      list.reload

      expect {
        list.tasks.where(:completed => true).last.destroy
        list.reload
      }.to change(list, :completed_tasks_count).by(-1)
    end
  end
end
