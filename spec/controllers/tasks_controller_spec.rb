require "spec_helper"

describe Api::V1::TasksController do
  describe "GET index" do
    it "should get index" do
      list = FactoryGirl.create :list, :with_tasks
      get :index, list_id: list.id
      assert_response :success
    end

    it "should return all tasks from list"do
      list = FactoryGirl.create :list, :with_tasks
      get :index, list_id: list.id
      body = JSON.parse response.body
      body["tasks"].should have(list.tasks.length).items
    end
  end

  describe "POST create" do
    it "should create new task" do
      list = FactoryGirl.create :list, :with_tasks

      expect {
        post :create, task: FactoryGirl.attributes_for(:task, list_id: list.id), list_id: list.id
      }.to change(list.tasks, :count).by(1)
    end
  end

  describe "DELETE destroy" do
    it "should destroy task" do
      list = FactoryGirl.create :list, :with_tasks

      expect {
        puts Task.count
        delete :destroy, list_id: list.id, id: list.tasks.first.id
      }.to change(Task, :count).by(-1)
    end
  end
end
