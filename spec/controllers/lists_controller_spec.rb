require "spec_helper"

describe Api::V1::ListsController do
  describe "GET index" do
    it "should get index" do
      get :index
      assert_response :success
    end

    it "should return all lists" do
      lists = FactoryGirl.create_list :list, 5
      get :index
      body = JSON.parse response.body
      body["lists"].should have(List.count).items
    end
  end

  describe "GET show" do
    it "should get list" do
      list = FactoryGirl.create :list
      get :show, id: list
      assert_response :success
    end

    it "should return correct list" do
      list = FactoryGirl.create :list
      get :show, id: list
      body = JSON.parse response.body
      body['list']['id'].should eq list.get_uuid
    end
  end

  describe "POST create" do
    it "should create new list" do
      expect {
        post :create
      }.to change(List, :count).by(1)
    end
  end
end
