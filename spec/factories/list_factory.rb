FactoryGirl.define do
  factory :list do
    sequence(:title) {|n| "List #{n}"}

    trait :without_title do
      title nil
    end

    trait :with_tasks do
      after(:create) do |list|
        FactoryGirl.create_list :task, 5, :list => list
        list.reload
      end
    end
  end
end
