FactoryGirl.define do
  factory :task do
    list
    sequence(:content) {|n| "Task Content #{n}"}
    quadrant 1
    completed false
  end 
end
