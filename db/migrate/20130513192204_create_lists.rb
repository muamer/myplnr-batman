class CreateLists < ActiveRecord::Migration
  def change
    create_table :lists, :id => false do |t|
      t.uuid :id, :primary_key => true
      t.string :title
      t.timestamps
    end
  end
end
