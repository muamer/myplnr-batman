class RemoveCounterCacheFromList < ActiveRecord::Migration
  def change
    remove_column :lists, :tasks_count
    remove_column :lists, :completed_tasks_count
  end
end
