class CreateQuadrant < ActiveRecord::Migration
  def change
    create_table :quadrants do |t|
      t.integer :index
      t.uuid :list_id, :null => false

      t.timestamps
    end
    add_index :quadrants, :list_id
  end
end
