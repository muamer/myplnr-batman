class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :content
      t.uuid :list_id, :null => false
      t.integer :quadrant, :default => 1

      t.timestamps
    end
    add_index :tasks, :list_id
  end
end
