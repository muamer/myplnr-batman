class AddCompletedTasksCountToLists < ActiveRecord::Migration
  def change
    add_column :lists, :completed_tasks_count, :integer, :default => 0

    List.reset_column_information
    List.all.each do |list|
      List.update_counters list.id, :completed_tasks_count => list.tasks.where(:completed => true).count
    end
  end
end
