class AddSortToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :sort, :integer, :default => Time.now.to_i
  end
end
