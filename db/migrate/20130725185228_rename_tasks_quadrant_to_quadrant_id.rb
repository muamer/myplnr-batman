class RenameTasksQuadrantToQuadrantId < ActiveRecord::Migration
  def change
    rename_column :tasks, :quadrant, :quadrant_id
  end
end
