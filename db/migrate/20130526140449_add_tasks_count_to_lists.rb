class AddTasksCountToLists < ActiveRecord::Migration
  def change
    add_column :lists, :tasks_count, :integer, :default => 0

    List.reset_column_information
    List.all.each do |list|
      List.update_counters list.id, :tasks_count => list.tasks.count
    end
  end
end
