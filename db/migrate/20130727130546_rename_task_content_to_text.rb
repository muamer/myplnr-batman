class RenameTaskContentToText < ActiveRecord::Migration
  def change
    rename_column :tasks, :content, :text
  end
end
